let segundos = 0
let minutos = 0
let horas = 0
let dias = 0
console.log('Hello citizen of mars!!!');
const contador = () => {
    const intervalo = 5
    segundos = intervalo + segundos
    if (segundos === 60) {
        minutos++
        segundos = 0
    }
    if (minutos === 60) {
        horas++
        minutos = 0
    }
    if (horas === 24) {
        dias++
        horas = 0
    }
    console.log(`The destruction of the earth has begun: ${dias} días ${horas} hour ${minutos} minutes ${segundos} seconds ago`);
}

setInterval(contador, 5000)