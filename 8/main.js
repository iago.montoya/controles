const numUsuarios = 10;
const url = 'https://randomuser.me/api/?results=' + numUsuarios
console.log(url)
const axios = require('axios')
// {nombre apellido sexo pais mail y enlace a foto}
resultados = []

async function getData() {
    const datos = await axios.get(url)
    let persona = {}
    let arrayDatos = datos.data.results.map(item => {
        persona.username = item.login.username
        persona.nombre = item.name.first
        persona.apellido = item.name.last
        persona.sexo = item.gender
        persona.pais = item.nat
        persona.mail = item.email
        persona.picture = item.picture.thumbnail
        return persona
    })
    console.log(arrayDatos)
}


getData()